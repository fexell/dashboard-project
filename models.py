from flask_mongoengine import MongoEngine
from flask_login import UserMixin

db = MongoEngine()

class User(db.Document, UserMixin):
    username = db.StringField(
        unique = True,
        required = True
    )
    email = db.EmailField(
        unique = True,
        required = True
    )
    password = db.StringField(
        required = True
    )
    firstname = db.StringField(
        default = ''
    )
    surname = db.StringField(
        default = ''
    )
    address = db.StringField(
        default = ''
    )
    access_level = db.IntField(
        default = 1,
        required = True
    )
    
    meta = {
        'indexes': [
            {
                'fields': [
                    'username',
                    'email'
                ],
                'unique': True,
                'collation': {
                    'locale': 'en',
                    'strength': 2
                }
            }
        ]
    }