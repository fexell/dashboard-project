
app = document.querySelector('#app')
toggleSidebarButton = document.querySelector('#SidebarToggleButtonContainer > button')

if(Cookies.get('sidebar-collapsed')) {
    app.classList.add('sidebar-collapsed')
}

toggleSidebarButton.addEventListener('click', function(e) {
    if (!Cookies.get('sidebar-collapsed')) {
        Cookies.set('sidebar-collapsed', 1, { path: '/' })
    } else {
        Cookies.remove('sidebar-collapsed')
    }

    app.classList.toggle('sidebar-collapsed')
})