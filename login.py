from flask import flash, redirect, url_for
from flask_login import LoginManager

login_manager = LoginManager()

@login_manager.unauthorized_handler
def unauthorized_callback():
    flash('You need to be logged in to access that route.', 'warning')
    return redirect(url_for('auth.login'))