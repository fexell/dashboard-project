from flask import Blueprint, redirect, render_template, request, flash, url_for
from flask_wtf import FlaskForm
from flask_login import login_user, login_required, logout_user, current_user
from wtforms import validators, EmailField, PasswordField, StringField, HiddenField, IntegerField
from wtforms.validators import DataRequired
from werkzeug.security import generate_password_hash, check_password_hash

from models import User

import re

admin = Blueprint('admin', __name__)

class AddUserForm(FlaskForm):
    username = StringField('Username', validators = [
        DataRequired(),
        validators.Regexp('^\w+$', message = 'Username can only contain letters, numbers and underscores.')
    ])
    email = EmailField('Email Address', validators = [
        DataRequired(),
        validators.Length(min = 6, max = 35)
    ])
    password = PasswordField('Password', validators = [
        DataRequired(),
        validators.Length(min = 4, max = 72)
    ])
    firstname = StringField('First Name', validators = [
        validators.Length(max = 40),
        validators.Regexp('^$|^\w+$', message = 'First name can only contain letters, numbers and underscores.')
    ])
    surname = StringField('Surname', validators = [
        validators.Length(max = 40),
        validators.Regexp('^$|^\w+$', message = 'Surname can only contain letters, numbers and underscores.')
    ])
    address = StringField('Address', validators = [
        validators.Length(max = 50)
    ])
    access_level = IntegerField('Access Level', default = 1, validators = [
        DataRequired(),
        validators.NumberRange(min = 1, max = 3)
    ])
    
class ChangeUserInformationForm(FlaskForm):
    username = StringField('Username', validators = [
        DataRequired(),
        validators.Regexp('^\w+$', message = 'Username can only contain letters, numbers and underscores.')
    ])
    email = EmailField('Email Address', validators = [
        DataRequired()
    ])
    firstname = StringField('First Name', validators = [
        validators.Length(max = 40),
        validators.Regexp('^$|^\w+$', message = 'First name can only contain letters, numbers and underscores.')
    ])
    surname = StringField('Surname', validators = [
        validators.Length(max = 40),
        validators.Regexp('^$|^\w+$', message = 'Surname can only contain letters, numbers and underscores.')
    ])
    address = StringField('Address', validators = [
        validators.Length(max = 50)
    ])
    access_level = IntegerField('Access Level', default = 1, validators = [
        DataRequired(),
        validators.NumberRange(min = 1, max = 3)
    ])
    
class DeleteUserForm(FlaskForm):
    id = HiddenField('ID')

@admin.route('/admin/users/')
@login_required
def admin_users():
    
    # If the current user doesn't have the required access level, display a warning
    if not current_user.access_level >= 2:
        flash('You do not have the access level to enter that route.', 'warning')
        return redirect(url_for('auth.dashboard'))
    
    users = User.objects()
    
    return render_template('admin/users.html', users = users)

@admin.route('/admin/users/<id>/', methods = ['GET', 'POST'])
@login_required
def display_user(id):
    if not current_user.access_level >= 2:
        flash('You do not have the access level to enter that route.', 'warning')
        return redirect(url_for('auth.dashboard'))
        
    form = ChangeUserInformationForm(request.form)
    user = User.objects(id = id).first()
    
    if request.method == 'POST' and form.validate():
        username = form.username.data
        email = form.email.data.lower()
        firstname = form.firstname.data.capitalize()
        surname = form.surname.data.capitalize()
        address = form.address.data
        access_level = form.access_level.data
        
        if not username and not email and not access_level:
            flash('Username, email and access level cannot be left empty.')
            return redirect(url_for('admin.display_user', id = id))
        
        elif re.match('^(admin)$', user.username, re.IGNORECASE) and user.username != username:
            flash('Cannot change the name of the admin account.', 'warning')
            return redirect(url_for('admin.display_user', id = id))
        
        # Check is the username has changed...
        elif user.username != username:
            for u in User.objects():
                # ... and then check if the username is already in use.
                if re.match('^' + u.username + '$', username, re.IGNORECASE):
                    flash('Username is already in use.', 'error')
                    return redirect(url_for('admin.display_user', id = id))
        
        # If no information is changed, display a warning
        elif user.username == username and user.email == email and user.firstname == firstname \
            and user.surname == surname and user.address == address and user.access_level == access_level:
                flash('You did not change any information.', 'warning')
                return redirect(url_for('admin.display_user', id = id))
        
        # If trying to change Admin's username, display a warning
        elif re.match('^(admin)$', user.username, re.IGNORECASE) and not re.match('^(admin)$', username, re.IGNORECASE):
            flash('You cannot change admin\'s username.', 'warning')
            return redirect(url_for('admin.display_user', id = id))
        
        # If trying to change Admin's access level to below 3, display a warning
        elif re.match('^(admin)$', user.username, re.IGNORECASE) and access_level != 3:
            flash('You cannot change the access level for admin.', 'warning')
            return redirect(url_for('admin.display_user', id = id))
            
        try:
            User.objects(id = id).update_one(
                username = username,
                email = email,
                firstname = firstname,
                surname = surname,
                address = address,
                access_level = access_level
            )
            
            flash(f'User has been updated.', 'success')
            return redirect(url_for('admin.display_user', id = id))
        
        except Exception as e:
            flash(f'{ e }', 'error')
            return redirect(url_for('admin.display_user', id = id))   
    
    return render_template('admin/display_user.html', user = user, form = form)

@admin.route('/admin/users/<id>/delete/', methods = ['POST'])
@login_required
def delete_user(id):
    if current_user.access_level != 3:
        flash('You do not have the access level to enter that route.', 'warning')
        return redirect(url_for('auth.dashboard', id = id))
    
    user = User.objects(id = id).first()
    
    # If trying to delete Admin account, display a warning
    if re.match('^(admin)$', user.username, re.IGNORECASE):
        flash('Cannot delete admin account.', 'warning')
        return redirect(url_for('admin.display_user', id = id))
    
    try:
        User.objects(id = id).delete()
        
        flash('Successfully deleted user.', 'success')
        return redirect(url_for('admin.admin_users'))

    except Exception as e:
        flash(f'{ e }')
        return redirect(url_for('admin.admin_users'))

@admin.route('/admin/users/add/', methods = ['GET', 'POST'])
@login_required
def add_user():
    if current_user.access_level != 3:
        flash('You do not have the access level to enter that route.', 'warning')
        return redirect(url_for('auth.dashboard'))
    
    form = AddUserForm(request.form)
    
    if request.method == 'POST' and form.validate():
        username = form.username.data
        email = form.email.data.lower()
        firstname = form.firstname.data.capitalize()
        surname = form.surname.data.capitalize()
        address = form.address.data
        password = generate_password_hash(form.password.data)
        access_level = form.access_level.data
        
        if not username or not email or not password or not access_level:
            flash('Username, email, password, and access level are required fields, and cannot be left empty.')
            return redirect(url_for('admin.add_user'))
        
        for user in User.objects():
            if re.match('^' + user.username + '$', username, re.IGNORECASE):
                flash('Username already in use.', 'error')
                return redirect(url_for('admin.add_user'))
        
        user = User(
            username = username,
            email = email,
            firstname = firstname,
            surname = surname,
            address = address,
            password = password,
            access_level = access_level
        )
        
        try:
            user.save()
            flash('Successfully added user.', 'success')
            return redirect(url_for('admin.add_user'))
        
        except Exception as e:
            flash(f'{ e }')
            return redirect(url_for('admin.add_user'))
    
    return render_template('admin/add_user.html', form = form)