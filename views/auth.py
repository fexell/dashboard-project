from flask import Blueprint, redirect, render_template, request, url_for, flash
from flask_wtf import FlaskForm
from flask_login import login_user, login_required, logout_user, current_user
from wtforms import validators, EmailField, PasswordField, StringField
from wtforms.validators import DataRequired
from werkzeug.security import generate_password_hash, check_password_hash

from login import login_manager

from models import User

import re
import json

auth = Blueprint('auth', __name__)

@login_manager.user_loader
def load_user(user_id):
    return User.objects(pk = user_id).first()

class LoginForm(FlaskForm):
    email = EmailField('Email Address', validators = [
        DataRequired()
    ])
    password = PasswordField('Password', validators = [
        DataRequired()
    ])
    
class SettingsForm(FlaskForm):
    username = StringField('Username', render_kw = { 'readonly': True }, validators = [
        DataRequired(),
        validators.Regexp('^\w+$', message = 'Username can only contain letters, numbers and underscores.')
    ])
    email = EmailField('Email Address', render_kw = { 'readonly': True }, validators = [
        DataRequired()
    ])
    firstname = StringField('First Name', validators = [
        validators.Length(max = 40),
        validators.Regexp('^$|^\w+$', message = 'First name can only contain letters, numbers and underscores.')
    ])
    surname = StringField('Surname', validators = [
        validators.Length(max = 40),
        validators.Regexp('^$|^\w+$', message = 'Surname can only contain letters, numbers and underscores.')
    ])
    address = StringField('Address', validators = [
        validators.Length(max = 50)
    ])
    
class ChangePasswordForm(FlaskForm):
    password = PasswordField('Current Password', validators = [
        DataRequired(),
        validators.Length(min = 4, max = 72)
    ])
    new_password = PasswordField('New Password', validators = [
        DataRequired(),
        validators.Length(min = 4, max = 72)
    ])
    
class SearchForm(FlaskForm):
    search = StringField('Search', validators = [
        DataRequired()
    ])
    
@auth.route('/search/')
@login_required
def search():
    term = request.args.get('search')
    search_result = []
    
    for item in User.objects:
        for i in item:
            if re.search(term, str(item[i]), re.IGNORECASE) and i != 'password':
                search_result.append(item[i])
    
    return render_template('search.html', search_result = search_result)
    
@auth.route('/dashboard/')
@login_required
def dashboard():
    return render_template('auth/dashboard.html')

@auth.route('/logout/')
@login_required
def logout():
    logout_user()
    return redirect(url_for('auth.login'))

@auth.route('/login/', methods = ['GET', 'POST'])
def login():
    
    if current_user.is_authenticated:
        return redirect(url_for('auth.dashboard'))
    
    form = LoginForm(request.form)
    
    if request.method == 'POST' and form.validate():
               
        email = form.email.data
        password = form.password.data
        
        user = User.objects(email = email).first()
        
        if user:
            if check_password_hash(user['password'], password):
                login_user(user)
                return redirect(url_for('auth.dashboard'))
            
            else:
                flash('Wrong password.', 'error')
                return redirect(url_for('auth.login'))
    
    return render_template('auth/login.html', form = form)

@auth.route('/signup/', methods = ['POST'])
def signup():
    username = request.form['username'].capitalize()
    email = request.form['email'].lower()
    password = request.form['password']
    
    if not username or not email or not password:
        return 'Username, email, and password are required.'
    
    elif not re.match('^\w+$', username, re.IGNORECASE) or not re.match('^\w+$', email, re.IGNORECASE):
        return 'Username, and email, can only contain of letters, numbers, and underscores.'
    
    elif not re.match('(.*){0,72}', password):
        return 'Password can only be 72 characters in length.'
    
    # Check whether username and/or email already exists.
    for u in User.objects():
        if re.match('^' + u.username + '$', username, re.IGNORECASE):
            return 'Username already in use.'
        
        elif re.match('^' + u.email + '$', email, re.IGNORECASE):
            return 'Email already in use.'
    
    user = User(
        username = username,
        email = email,
        password = generate_password_hash(password)
    )
    
    try:
        user.save()
        return f'User { user.username } successfully added!'
    
    except Exception as e:
        return e
    
@auth.route('/settings/')
def settings():
    return redirect(url_for('auth.user_settings'))

# User paths/routes
@auth.route('/user/settings/', methods = ['GET', 'POST'])
@login_required
def user_settings():
    
    form = SettingsForm(request.form)
    
    if request.method == 'POST' and form.validate():
        username = form.username.data
        email = form.email.data.lower()
        firstname = form.firstname.data.capitalize()
        surname = form.surname.data.capitalize()
        address = form.address.data
        
        if current_user.username == username and current_user.email == email and current_user.firstname == firstname \
            and current_user.surname == surname and current_user.address == address:
                flash('You did not change any information.', 'warning')
                return redirect(url_for('auth.user_settings'))
        
        elif username != current_user.username or email != current_user.email:
            flash('Only the administrator can change username and/or email.', 'warning')
            return redirect(url_for('auth.user_settings'))
        
        try:
            User.objects(id = current_user.id).update_one(
                firstname = firstname,
                surname = surname,
                address = address
            )
            
            flash('Information successfully updated.', 'success')
            return redirect(url_for('auth.user_settings'))
            
        except Exception as e:
            flash(f'{ e }')
            return redirect(url_for('auth.user_settings'))
    
    return render_template('user/settings/user_settings.html', form = form)

@auth.route('/user/settings/password/', methods = ['GET', 'POST'])
@login_required
def change_password():
    
    form = ChangePasswordForm(request.form)
    
    if request.method == 'POST' and form.validate():
        current_password = form.password.data
        new_password = form.new_password.data
        
        if current_password == new_password:
            flash('You entered your current password as the new password.', 'warning')
            return redirect(url_for('auth.change_password'))
        
        if check_password_hash(current_user.password, current_password):
            User.objects(id = current_user.id).update_one(
                password = generate_password_hash(new_password)
            )
            flash('Password successfully updated.', 'success')
            return redirect(url_for('auth.change_password'))
        
        else:
            flash('Wrong current password.', 'error')
            return redirect(url_for('auth.change_password'))
    
    return render_template('user/settings/change_password.html', form = form)

@auth.route('/user/<name>')
@login_required
def user_profile(name):
    
    user = User.objects(username = name).first()
    
    return render_template('user/profile.html', name = name, user = user)