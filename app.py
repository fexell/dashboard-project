from flask import Flask

from login import login_manager

from models import db

from views.auth import auth
from views.main import main
from views.admin import admin

def create_app():
    app = Flask(__name__)
    
    app.config['SECRET_KEY'] = 'secret'
    app.config['MONGODB_DB'] = 'dashboard-project'
    app.config['MONGODB_HOST'] = 'localhost'
    app.config['MONGODB_PORT'] = 27017
    app.config['DEBUG'] = True
    app.config['TESTING'] = True
    
    with app.app_context():
        db.init_app(app)
        login_manager.init_app(app)
    
    app.register_blueprint(auth)
    app.register_blueprint(main)
    app.register_blueprint(admin)
    
    return app