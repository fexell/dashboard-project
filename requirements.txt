bidict==0.22.0
certifi==2022.6.15
charset-normalizer==2.0.12
click==8.1.3
colorama==0.4.5
dnspython==2.2.1
docopt==0.6.2
email-validator==1.2.1
Flask==2.1.2
Flask-Login==0.6.1
flask-mongoengine==1.0.0
Flask-SocketIO==5.2.0
Flask-WTF==1.0.1
idna==3.3
itsdangerous==2.1.2
Jinja2==3.1.2
MarkupSafe==2.1.1
mongoengine==0.24.1
pipreqs==0.4.11
pymongo==4.1.1
python-dotenv==0.20.0
python-engineio==4.3.2
python-socketio==5.6.0
requests==2.28.0
urllib3==1.26.9
Werkzeug==2.1.2
WTForms==3.0.1
yarg==0.1.9
